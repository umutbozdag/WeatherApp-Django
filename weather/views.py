import requests
from django.shortcuts import render
from .models import City
from .forms import CityForm

def index(request):
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&lang=tr&appid=e792e9d1044edc47e53443e8c6756cdd&units=metric'

    if request.method == 'POST':
        form = CityForm(request.POST)
        form.save()
        

    form = CityForm()

    cities = City.objects.all()

    weather_data = []

    for city in cities:

    # verileri request yaparak apiden aldık
        r = requests.get(url.format(city)).json() 
    
        city_weather = {
            'city' : city.name,
            'temperature': r['main']['temp'] , # api'den sıcaklık bilgisini aldık
            'description' : r['weather'][0]['description'] , # api'den hava durumu açıklamasını aldık
            'icon': r['weather'][0]['icon'] , # api'den ikonu aldık
        }

        weather_data.append(city_weather)
    
    
    context = {'weather_data': weather_data, 'form': form}
    return render(request, 'weather/weather.html', context)
